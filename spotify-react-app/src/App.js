import React, { useEffect, useState } from 'react';
import './styles/principal.css';
import ArtistSearch from './artists_search';
import { getAccessToken, getArtists, getArtistsByName } from './services/spotify_api';
import { clientId, clientSecret, ARTISTS_IDS } from './services/constants';
import { Link } from 'react-router-dom';

const Principal = () => {
  const [accessToken, setAccessToken] = useState(null);
  const [artists, setArtists] = useState([]);
  const [searchText, setSearchText] = useState("");
  const [clickedArtists, setClickedArtists] = useState([]);

  useEffect(() => {
    const fetchAccessToken = async () => {
      try {
        const token = await getAccessToken(clientId, clientSecret);
        setAccessToken(token);
      } catch (error) {
        console.error('Error retrieving access token:', error);
      }
    };

    fetchAccessToken();
  }, []);

  const handleSearch = async (text) => {
    setSearchText(text);
  };

  useEffect(() => {
    const fetchArtists = async () => {
      try {
        let newArtists = [];
        if (accessToken) {
          newArtists = await getArtists(accessToken, ARTISTS_IDS);
        }
        if (searchText) {
          const artistsByName = await getArtistsByName(accessToken, [searchText]);
          newArtists = newArtists.concat(artistsByName);
        }
        setArtists(newArtists);
      } catch (error) {
        console.error('Error retrieving artists:', error);
      }
    };
    fetchArtists();
  }, [accessToken, searchText]);

  const handleArtistClick = (artistId) => {
    if (clickedArtists.includes(artistId)) {
      setClickedArtists(clickedArtists.filter((id) => id !== artistId));
    } else {
      setClickedArtists([...clickedArtists, artistId]);
    }
  };

  return (
    <div className = "App">
      <header className = "Header">
        <div className = "Search">
          <ArtistSearch accessToken = {accessToken} onSearch = {handleSearch} />
        </div>
      </header>
      <h1 className = "Title">Top Artist</h1>
      <div className = "Grid">
        {artists ? (
          artists.map((artist) => (
            <div className = "Card" key = {artist.id}>
              <Link to = {`/artist/${artist.id}`} key = {artist.id}>
                <div className = "ImageContainer">
                  <img className = "ArtistImage" src = {artist.images[0].url} alt = {artist.name} />
                </div>
                <p
                  className = {`ArtistName ${clickedArtists.includes(artist.id) ? 'Clicked' : ''}`}
                  onClick = {() => handleArtistClick(artist.id)}
                >
                  {artist.name}
                </p>
                <p className = "ArtistTitle">Artist</p>
              </Link>
            </div>
          ))
        ) : (
          <p>No artists found</p>
        )}
      </div>
    </div>
  );
};

export default Principal;
