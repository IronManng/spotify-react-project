import { HOST_API, HOST_AUTH} from '../services/constants';

export const handleResponse = (response) => {
  return response.text().then((text) => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }
    return data;
  });
};

export const getAccessToken = async (clientId, clientSecret) => {
  const headers = new Headers();
  headers.append("Content-Type", "application/x-www-form-urlencoded");

  const urlencoded = new URLSearchParams();
  urlencoded.append("grant_type", "client_credentials");
  urlencoded.append("client_id", clientId);
  urlencoded.append("client_secret", clientSecret);

  const requestOptions = {
    method: 'POST',
    headers: headers,
    body: urlencoded,
  };

  const response = await fetch(`${HOST_AUTH}/api/token`, requestOptions);

  try {
    const data = await response.json();
    return data['access_token'];

  } catch (error) {
    if (response.status === 401) {
      const newToken = await getAccessToken(clientId , clientSecret);
      return newToken;
    } else {
    console.log('error', error);
  }
 }
};

export const getArtists = async (accessToken, artistsIds) => {
  const headers = new Headers();
  headers.append("Authorization", `Bearer ${accessToken}`);
  const requestOptions = {
    method: 'GET',
    headers: headers,
  };
  const idsParams = artistsIds.join();
  const response = await fetch(`${HOST_API}/artists?ids=${idsParams}`, requestOptions);
  try {
    const data = await handleResponse(response);
    return data['artists'];

  } catch (error) {
    console.log('error', error);
  }
};

export const getArtistsByName = async (accessToken, artistNames) => {
  const headers = new Headers();
  headers.append("Authorization", `Bearer ${accessToken}`);
  const requestOptions = {
    method: 'GET',
    headers: headers,
  };
  const searchPromises = artistNames.map(async (name) => {
    const response = await fetch(`${HOST_API}/search?q=${encodeURIComponent(name)}&type=artist`, requestOptions);
    const data = await handleResponse(response);
    return data['artists']['items'][0];
  });
  const artists = await Promise.all(searchPromises);
  return artists.filter(artist => artist !== undefined);
};


export const getTopTracks = async (accessToken, artistsId) => {
  console.log('accessToken', accessToken);
  console.log('artistsId', artistsId);
  const headers = new Headers();
  headers.append("Authorization", `Bearer ${accessToken}`);
  const requestOptions = {
    method: 'GET',
    headers: headers,
  };
  const response = await fetch(`${HOST_API}/artists/${artistsId}/top-tracks?market=CO`, requestOptions);
  try {
    const data = await handleResponse(response);
    return data['tracks'];

  } catch (error) {
    console.log('error', error);
  }
};

export default getAccessToken;
