const clientId = '98e668d418554d5dbb2b85d07e53adcf';
const clientSecret = '819b81803d3a4ed193723b2d29913d41';

const HOST_AUTH = "https://accounts.spotify.com"
const HOST_API = "https://api.spotify.com/v1"

const ARTISTS_IDS = 
['1vyhD5VmyZ7KMfW5gqLgo5',
  '0XwVARXT135rw8lyw1EeWP',
  '790FomKkXshlbRYZFtlgla',
  '4gzpq5DPGxSnKTe4SA8HAU',
  '04gDigrS5kc9YWfZHwBETP',
  '5YGY8feqx7naU7z4HrwZM6',
  '2LRoIwlKmHjgvigdNGBHNo',
  '2OHKEe204spO7G7NcbeO2o',
  '28gNT5KBp7IjEOQoevXf9N',
  '7FsRH5bw8iWpSbMX1G7xf1',
  '6FBDaR13swtiWwGhX1WQsP',
  '4ssUf5gLb1GBLxi1BhPrVt',
];

export {clientId, clientSecret, ARTISTS_IDS, HOST_AUTH, HOST_API};