import React, { useState } from 'react';
import { getArtists } from './services/spotify_api';
import lupaImage from './Images/lupa.png';

const ArtistSearch = ({accessToken, onSearch }) => {
  const [searchText, setSearchText] = useState('');

  const handleSearchChange = (event) => {
    setSearchText(event.target.value);
  };

  const handleSearch = async () => {
    try {
      const artists = await getArtists(accessToken, [searchText]);
      onSearch(artists);
    } catch (error) {
      console.log('Error al realizar la búsqueda:', error);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      event.preventDefault();
      handleSearch();
    }
  };

  return (
    <div className="Search">
      <img src={lupaImage} alt='Lupa' className='SearchIcon' />
      <input
        type="text"
        value={searchText}
        onChange={handleSearchChange}
        onKeyPress={handleKeyPress}
        className="SearchInput"
        placeholder='What do you want to listen?'
      />
      <p className="SearchText" onClick={() => handleSearch(accessToken)}/>
    </div>
  );
};

export default ArtistSearch;
