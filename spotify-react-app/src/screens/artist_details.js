import React, { useState, useEffect } from 'react';
import { getArtists, getTopTracks, getAccessToken } from '../services/spotify_api';
import { clientId, clientSecret } from '../services/constants';
import { useParams } from 'react-router-dom';
import '../styles/details.css';

const ArtistDetails = ({ match }) => {
  const [artist, setArtist] = useState(null);
  const [topTracks, setTopTracks] = useState([]);
  const params = useParams();
  const artistId = params.id;

  useEffect(() => {
    const fetchArtistDetails = async () => {

      try {
        const accessToken = await getAccessToken(clientId, clientSecret);
        const artistDetails = await getArtists(accessToken, [artistId]);
        const tracks = await getTopTracks(accessToken, artistId);

        setArtist(artistDetails[0]);
        setTopTracks(tracks);
      } catch (error) {
        console.error('Error retrieving artist details:', error);
      }
    };

    fetchArtistDetails();
  }, [artistId]);

  return (
    <div className = "ArtistDetails">
      {artist && (
        <div className = "ArtistContainer">
          <div className = "ArtistImageContainer">
            <div className = "ArtistImageBackground"></div>
            <img className = "ArtistImage1" src = {artist.images[0].url} alt = {artist.name} />
          </div>
          <h2 className = "ArtistName1">{artist.name}</h2>
        </div>
      )}
  
  <h3 className = "TopTracksTitle">Top Tracks</h3>
      <ul className = "TrackList">
        {topTracks.map((track, index) => (
          <li className = "TrackItem" key = {track.id}>
            <span className = "TrackNumber">{index + 1}</span>
            <img className = "AlbumImage" src = {track.album.images[0].url} alt = {track.album.name} />
            <span className = "TrackName">{track.name}</span>
          </li>
        ))}
      </ul>
    </div>
  );  
};

export default ArtistDetails;
