import React from 'react';
import './index.css';
import ReactDOM from 'react-dom/client';
import Principal from './App';
import ArtistDetails from './screens/artist_details';
import reportWebVitals from './reportWebVitals';
import { Route, Routes, BrowserRouter } from 'react-router-dom';

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>
);

function App() {
  return (
    <div className='App'>
      <Routes>
        <Route path='/' element={<Principal/>} />
        <Route path='/artist/:id' element={<ArtistDetails/>}/>
      </Routes>
    </div>
  )
}


export default App

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
