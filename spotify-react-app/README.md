# Spotify React Project

This project is a React application that displays artist cards using the Spotify API.

## Requirements

Before running the project, make sure you have the following requirements:

Node.js installed on your machine.
Spotify Developer Account to obtain the necessary credentials.

## Features
Displays artist cards with their image and name.
Search bar to search for specific artists.

## Screenshots
Below are some screenshots of the application:

![GIF](screenshots/spotify_react_video.mp4)
![screenshot1](screenshots/screenshot5.png)
![screenshot2](screenshots/Screenshot1.png)


## Responsive Design
The application is designed to be responsive and adapts to different screen sizes. Here's a preview of the responsive design:

![responsivedesing1](screenshots/screenshot2.png)
![responsivedesing1](screenshots/screenshot3.png)
![responsivedesing1](screenshots/screenshot4.png)

## Installation

1. Clone this repository to your local machine:

git clone https://gitlab.com/IronManng/spotify-react-project.git

2. Navigate to the project directory:

cd spotify-react-project-app

3. Install the project dependencies:

npm install

## Execution
1. Start the application in development mode:

npm start

2. Open your web browser and go to http://localhost:3000 to see the application in action.

3. To access the Spotify API, the necessary credentials are provided in the spotify_api.js file. No additional steps are required to create a new app in the Spotify Developer Dashboard.

## Folder Structure
The folder structure used in this project is as follows:

* public: Contains public static files.
* src: Contains the source code of the application.
* services: Contains services for interacting with the Spotify API.
* styles: Contains CSS style files.
